# coding:utf-8
# 所谓迭代器，其实就是实现迭代协议的一个容器对象
# 实现该协议就是实现2个方法next和__iter__方法，next方法返回每次调用next()时候想要
# 返回的数据，而__iter__对象需要返回迭代器对象本身，即该容器对象

# 下面实现一个迭代器
class MyIter(object):
    def __init__(self, step):
        self.step = step

    def next(self):
        """返回下一个值"""
        ret = self.step
        if ret == 0:
            # 这里一定要定一个界限的时候，当到达这个界限的时候，抛出StopIteration异常，该异常会导致程序崩溃，所以必须捕获
            raise StopIteration
        else:
            self.step -= 1
            return ret

    def __iter__(self):
        return self


if __name__ == "__main__":
    it = MyIter(3)

    #先使用循环遍历it对象，for循环可以自动捕获StopIteration异常
    for step in it:
        print step

    #显式的使用next方法， 是需要手动捕获StopIteration异常，否则会导致程序崩溃
    try:
        it.next()
    except StopIteration:
        print "捕获了StopIteration异常"
    finally:
        pass