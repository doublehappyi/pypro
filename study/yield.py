# coding:utf-8
__author__ = 'db'
# 生成器其实就是一个保存了函数执行环境的一个对象，
# 当这个对象调用next方法的时候，就返回下一个yield后面的表达式的值
# 如果没有下一个yield，就会抛出StopIteration异常,
# 所以使用生成器的时候，一定要捕获该异常，否则抛出该异常时会导致程序崩溃

#生成器还有一个send方法，用于和生成器交互，传递参数给生成器
#生成器还可以主动抛出异常，以及主动关闭该生成器


# 生成器最经典的例子之一就是实现fabonacci数列
def fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

# 调用send方法的例子
def send():
    n = 0
    while 1:
        m = yield n
        print "m + n = %i" %  (m + n)
        n = m*m


if __name__ == "__main__":
    print "fibonacci数列调试"
    f = fibonacci()
    print "从0开始，打印前5个fobonacci数"
    for i in xrange(5):
        print "第%s个fobonacci数: %s" % (i, f.next())


    print "调用send方法的例子调试"

    s = send()
    print "n: %s" % s.next()
    print "n: %s" % s.send(2)
    print "n: %s" % s.send(3)




