def myfunc():
  print 'myfunc is called'

def deco(func):
  def wrapper(*args, **kwargs):
    print 'before func...'
    func(*args, **kwargs)
    print 'after func...'
  return wrapper


myfunc = deco(myfunc)
myfunc()
myfunc()
