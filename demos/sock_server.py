import socket
from threading import Thread
def server():
    address = ('127.0.0.1', 8000)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(address)
    s.listen(5)

    while True:
        conn, addr = s.accept()
        t = ThreadHandler(conn, addr)
        t.run()

    s.close()

class ThreadHandler(Thread):
    def __init__(self, conn, addr):
        self.conn = conn
        self.addr = addr

    def run(self):
        while True:
            msg = self.conn.recv(1024)
            print "msg: ", msg
            self.conn.send("welcome !" + msg)

        self.conn.close()

if __name__ == "__main__":
    server()