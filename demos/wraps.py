from functools import wraps

def my_decorator(f):
  @wraps(f)
  def wrapper(*args, **kwargs):
    print "calling decorated function"
    return f(*args, **kwargs)

  return wrapper

@my_decorator
def example():
  print "called in example function"

example()
